package com.qmetry.qaf.example.steps;



import java.util.List;

import org.testng.Assert;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class verifyWomensItem {
	
	@QAFTestStep(description = "enter the Women's shoes in the search field based on the search item {productName}")
	public void SearchWomensProduct(String productName) {
		CommonStep.click("txt.productsearch.homepage");
		CommonStep.sendKeys(productName, "txt.searchallcategory.homepage");
		Reporter.log(productName+" this is the product name");
		((AndroidDriver) CommonUtility.getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
	}
	@QAFTestStep(description="verify product should be displayed based on the women's shoes searched item")
	public void VerifyWomensShoes(){
		String productTitle = CommonStep.getText("txt.productTitle.productlistpage");
		Assert.assertTrue(productTitle.contains("Women's"), "product title contains the product name");
		Reporter.logWithScreenShot("product title contains the product name");
	
	}
}
