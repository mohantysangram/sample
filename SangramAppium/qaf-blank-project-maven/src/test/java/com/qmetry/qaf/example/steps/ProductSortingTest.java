package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;

public class ProductSortingTest {

	
	@QAFTestStep(description="use filter present on right side to sort")
	public void sorting_based_price_lowtogih(){
		CommonStep.click("amazon.filter");
		CommonStep.click("amazon.filter.sort");
		CommonStep.click("amazon.filter.sort.lowtohigh");
		CommonUtility.getDriver().navigate().back();
	}
	@QAFTestStep(description="verify the result displayed as per sorting")
	public void verify_sorted_result(){
		String lowest=CommonStep.getText("amazon.filter.sort.lowest");
		String actualprice1=lowest.substring(1, 3)+lowest.substring(4, 7);
		Reporter.log("--------------");
		Reporter.log(actualprice1);
		Reporter.log("--------------");
		String lowest2=CommonStep.getText("amazon.filter.sort.lowest2");	
		String actualprice2=lowest2.substring(1, 3)+lowest2.substring(4, 7);
		Reporter.log("--------------");
		Reporter.log(actualprice2);
		Reporter.log("--------------");
		int lowestprice=Integer.parseInt(actualprice1);
		Reporter.log(lowestprice+"");
		int secondlowestprice=Integer.parseInt(actualprice2);
		Reporter.log(secondlowestprice+"");
		if(lowestprice<secondlowestprice) {
			Reporter.log("Sorting is applied correctly");
		}
	}	
	
	
}
