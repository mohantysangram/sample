package com.qmetry.qaf.example.steps;

import io.appium.java_client.TouchAction;

import org.openqa.selenium.interactions.touch.TouchActions;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
 

public class Scroll extends WebDriverTestCase {
	
	
	Scroll() {		
		AppiumDriver driver = (AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		QAFWebElement e = getDriver().findElement("amazon.addto.cart");		
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(e, 6000).moveTo(1, 1).release().perform();
	}
	public void scroll(){
		AppiumDriver driver = (AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		QAFWebElement element = getDriver().findElement("amazon.addto.cart");
		TouchActions action = new TouchActions(driver);
		action.scroll(element, 10, 100);
		action.perform();
	}
	
/*	public  void scroll(double startPt, double endPt) {

	org.openqa.selenium.Dimension dim = (org.openqa.selenium.Dimension) getDriver().manage().window()
			.getSize();
	int screenwidth = (int) (dim.width / 2);
	int scrollingStartPoint = (int) (dim.getHeight() * startPt);
	int scrollingEndPoint = (int) (dim.getHeight() * endPt);
	@SuppressWarnings("all")
	TouchAction touchaction = new TouchAction(getDriver());
	touchaction.press(Point.point(screenwidth, scrollingStartPoint))
			.waitAction(WaitOptions.waitOptions(Duration.ofMillis(4000)))
			.moveTo(PointOption.point(screenwidth, scrollingEndPoint)).release().perform();
} */

}
