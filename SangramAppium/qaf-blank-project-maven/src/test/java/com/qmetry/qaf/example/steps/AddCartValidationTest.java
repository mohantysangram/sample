package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class AddCartValidationTest {

	@QAFTestStep(description = "select any product from product list page")
	public void search_For_dell_inspiron_laptop() {
		CommonStep.click("txt.productsearch.homepage");
		CommonStep.sendKeys("dell inspiron", "txt.searchallcategory.homepage");
		((AndroidDriver) CommonUtility.getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
		CommonStep.click("dell.inspiron.lap");;

	}

	@QAFTestStep(description = "tap on add to cart button")
	public static void add_firstBook_in_cart() {

		Reporter.log("now scrolling up to add to cart with the help of scroll class");
		new Scroll();
		CommonStep.click("amazon.addto.cart");

		String noofproductsincart = CommonStep.getText("amazon.cart");
		Reporter.log("-------------------------");
		int cartitem = Integer.parseInt(noofproductsincart);
		Reporter.log(cartitem + ": Products are in Cart");
		Reporter.log("-------------------------");
		if (cartitem > 0) {
			Reporter.log("First laptop is added to the cart");
		}
		CommonUtility.getDriver().navigate().back();
	}

	@QAFTestStep(description = "add another laptop to cart")
	public static void add_Second_Book_in_cart() {

		CommonStep.click("dell.inspiron.lap");
		Reporter.log("now scrolling up to add to cart with the help of scroll class");
		CommonUtility.scrollBy();
		CommonStep.click("amazon.addto.cart");
	}
	@QAFTestStep(description = "click on cart and verify the product count added")
	public static void verify_cart() {
		String noofproductsincart = CommonStep.getText("amazon.cart");
		Reporter.log("-------------------------");
		int cartitem = Integer.parseInt(noofproductsincart);
		Reporter.log(cartitem + ": Products are in Cart");
		Reporter.log("-------------------------");

		if (cartitem > 0) {
			Reporter.log("laptop is added to the cart");
		}
	}
}
