package com.qmetry.qaf.example.steps;


import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class RegisteringWithExistingUser {

	@QAFTestStep(description = "validate Indication message will be dispalyed for the Existing Users")
	public void VerifyMobileNumberDisplayed() {
			CommonStep.waitForPresent("txt.create.act.same.mobile");
			CommonStep.verifyPresent("txt.create.act.same.mobile");
		
	}

}
