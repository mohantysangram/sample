package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class FilteringTest {
	
	@QAFTestStep(description="enter the books in the search field based on the search item {productName}")
	public void SearchSelectedProduct(String productName){
		CommonStep.click("txt.productsearch.homepage");
		CommonStep.sendKeys(productName, "txt.searchallcategory.homepage");
		Reporter.log(productName+" this is the product name");
		((AndroidDriver) CommonUtility.getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
		
	}
	
	@QAFTestStep(description="tap on Filter button on right side")
	public void Tap_Filter(){
		CommonStep.click("dd.filterproduct.productlistpage");
		
	}
	@QAFTestStep(description = "select any filter from menu i.e. Format")
	public static void select_filter() {
		CommonStep.click("amazon.filter.format.productlistpage");
	}
	
	@QAFTestStep(description = "select any particular filter option from menu (i.e. Paperback)")
	public static void paperback_filter() {	
		CommonStep.click("amazon.filter.format.paperback.productlistpage");		
	}
	
	@QAFTestStep(description = "again tap on filter button to close filter menu")
	public static void close_filter() {
		CommonStep.click("dd.filterproduct.productlistpage");
	//((AndroidDriver) CommonUtility.getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
	}
		
	@QAFTestStep(description = "verify products displayed on page")
	public static void verify_product_displayed() {	
		
		String filterr=CommonStep.getText("amazon.filter.results.p.productlistpage");
		Reporter.log("---------------------------------------");
		Reporter.log(filterr);
		Reporter.log("---------------------------------------");
		CommonStep.verifyText("amazon.filter.results.p.productlistpage","Paperback");	
	}




}
