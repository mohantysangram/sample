package com.qmetry.qaf.example.steps;


import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class RegistrationTest {

	@QAFTestStep(description = "open the given Application")
	public void OpenApplication() {
		CommonStep.click("btn.skipsignin.homepage");
	}

	@QAFTestStep(description = "click on menu button and hello sign in link")
	public void ClickingOnSignInLink() {
		CommonStep.waitForPresent("txt.applogo.homepage");
		CommonStep.click("btn.menu.homepage");
		CommonStep.waitForPresent("btn.offcanvas.signin", 5000);
		CommonStep.click("btn.offcanvas.signin");
	}

	@QAFTestStep(description = "validate verify Welcome Page Should dispalyed")
	public void VerifyWelComePageDisplayed() {
		CommonStep.verifyPresent("txt.welcomelogo.welcomepage");
	}

	@QAFTestStep(description = "click on create account radio button")
	public void ClickOnCreateNewAccount() {
		CommonStep.verifyPresent("rad.createaccount.welcomepage");
		CommonStep.click("rad.createaccount.welcomepage");
	}

	@QAFTestStep(description = "enter the username {username} number {mblnumber} mail {mail} password {password}")
	public void CreateUser(String name,String phone,String email,String pwd) {
		CommonStep.sendKeys(name, "txt.name.registrationpage");
		CommonStep.sendKeys(phone, "txt.mobilenumber.registrationpage");
		CommonStep.sendKeys(email, "txt.email.registrationpage");
		CommonStep.sendKeys(pwd, "txt.setpassword.registrationpage");
	}

	@QAFTestStep(description = "click on verify mobile number button")
	public void ClickOnVerifyBtn() {
		CommonStep.waitForPresent("btn.verifymobilenumber.registrationpage",5000);
		CommonStep.click("btn.verifymobilenumber.registrationpage");
	}

	@QAFTestStep(description="validate verify mobile number page displayed successfully")
	public void validateVerifyMobileNumberPageDisplayedSuccessfully(){
		CommonStep.verifyPresent("txt.pagetitle.verifymobilenumberpage");
	}
}
