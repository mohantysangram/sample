package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;



import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;


public class StepsLibrary extends WebDriverTestBase {
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 */
	@QAFTestStep(description = "search for {0}")
	public static void searchFor(String searchTerm) {
		sendKeys(searchTerm, "input.search");
		click("button.search");
	}
	
	public static AndroidDriver driver;

	@QAFTestStep(description = "Navigates to back")
	public static void navigateBack() {
		new WebDriverTestBase().getDriver().navigate().back();
	}

	public  AndroidDriver getAndroidDriver() {
		driver = (AndroidDriver) getDriver().getUnderLayingDriver();
		return driver;

	}
/*
	public  void scroll(double startPt, double endPt) {

		org.openqa.selenium.Dimension dim = (org.openqa.selenium.Dimension) getAndroidDriver().manage().window()
				.getSize();
		int screenwidth = (int) (dim.width / 2);
		int scrollingStartPoint = (int) (dim.getHeight() * startPt);
		int scrollingEndPoint = (int) (dim.getHeight() * endPt);
		@SuppressWarnings("all")
		TouchAction touchaction = new TouchAction(getAndroidDriver());
		touchaction.press(PointOption.point(screenwidth, scrollingStartPoint))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(4000)))
				.moveTo(PointOption.point(screenwidth, scrollingEndPoint)).release().perform();
	} */
	
	
}
