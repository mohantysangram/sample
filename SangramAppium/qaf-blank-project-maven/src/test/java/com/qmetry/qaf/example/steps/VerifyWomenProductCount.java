package com.qmetry.qaf.example.steps;


import org.testng.Assert;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;

public class VerifyWomenProductCount {

	@QAFTestStep(description = "find the Total Number of Women's product present on the pages after filter")
	public void VerifyWomensshoes() {
		
		String productTitle = CommonStep.getText("txt.productTitle.productlistpage");
		Assert.assertTrue(productTitle.contains("Women's"), "product title contains the product name");
		Reporter.logWithScreenShot("product title contains the product name");

	}

	@QAFTestStep(description = "find the Total Number of Women's product present on the pages after filter")
	public void findTheTotalNumberOfWomenSProductPresentOnThePagesAfterFilter() {
	}

	@QAFTestStep(description = "verify All product should belong from women's category")
	public void VerifyWomensCountShoes() {

	}

}
