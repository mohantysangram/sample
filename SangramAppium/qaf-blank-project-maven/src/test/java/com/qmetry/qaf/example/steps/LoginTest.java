package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class LoginTest {

	@QAFTestStep(description = "enter the mail {email} password {password}")
	public void CreateNewAccount(String email,String pwd) {
		CommonStep.sendKeys(email, "txt.entermail.loginpage");
		CommonStep.click("btn.entercontinue.loginpage");
		CommonStep.waitForPresent("txt.enterpassword.loginpage",5000);
		CommonStep.sendKeys(pwd, "txt.enterpassword.loginpage");
	}
	
	@QAFTestStep(description = "click on Continue button")
	public void ClickOnContinue() throws InterruptedException {
		Thread.sleep(5000);
		CommonStep.waitForPresent("btn.enterlogin.loginpage",5000);
		CommonStep.click("btn.enterlogin.loginpage");
	}
	
	@QAFTestStep(description = "validate User should able to login into the application")
	public void ValidateUserLoggedIn() {
		CommonStep.waitForPresent("txt.applogo.homepage");
		CommonStep.click("btn.menu.homepage");
		CommonStep.verifyPresent("txt.actionbar.loginpage");
	}

}
