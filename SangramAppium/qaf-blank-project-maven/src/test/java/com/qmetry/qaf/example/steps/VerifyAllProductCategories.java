package com.qmetry.qaf.example.steps;


import org.testng.Assert;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class VerifyAllProductCategories {

	@QAFTestStep(description = "click on menu button")
	public void ClickOnMenuBar(){
		CommonStep.waitForPresent("txt.applogo.homepage");
	}
	
	@QAFTestStep(description = "click on shop by Category")
	public void clickOnShopCategory(){
		CommonStep.click("link.shopcategory.offcanvas");
	}
	
	@QAFTestStep(description = "click on fire Tv Stick {CategoryName} Fashion link and click on product {productName}")
	public void SelectAmazonFashionCategory(String categoryName,String productName){
		QAFExtendedWebElement  categories=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.allproducts.categorylistpage"), categoryName));
		categories.click();
		QAFExtendedWebElement  product=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.productname.categorylistpage"), productName));
		product.click();
	}
	
	@QAFTestStep(description = "validate the product {productTitle}")
	public void verifyproduct(String productTitle){
		QAFExtendedWebElement  categories=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.allproducts.categorylistpage"), productTitle));
		String getproductTitle=categories.getText();
		Reporter.log("product Title is :"+productTitle);
		Assert.assertTrue(getproductTitle.contains(productTitle), "product title contains the searched product name ");
		Reporter.logWithScreenShot("Navigated to the searched product page");
	}
}
