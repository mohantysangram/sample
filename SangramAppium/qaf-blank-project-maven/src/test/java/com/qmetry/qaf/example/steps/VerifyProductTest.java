package com.qmetry.qaf.example.steps;

import org.testng.Assert;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class VerifyProductTest {

	@QAFTestStep(description = "enter the value in the search field based on the search item {productName}")
	public void SearchProduct(String productName) {
		CommonStep.click("txt.productsearch.homepage");
		CommonStep.sendKeys(productName, "txt.searchallcategory.homepage");
		((AndroidDriver) CommonUtility.getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
	}

	@QAFTestStep(description = "verify product should be displayed beased on the searched item")
	public void VerifySearchProduct() {
		String productTitle = CommonStep.getText("txt.productTitle.productlistpage");
		Assert.assertTrue(productTitle.contains("Shoes"), "product title contains the product name");
		Reporter.logWithScreenShot("product title contains the product name");

	}
}
