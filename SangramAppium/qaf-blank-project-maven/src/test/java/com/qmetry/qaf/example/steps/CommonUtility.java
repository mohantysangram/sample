package com.qmetry.qaf.example.steps;



import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.touch.TouchActions;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

public class CommonUtility  {

	public static AppiumDriver getDriver(){
		AppiumDriver driver=(AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		return driver;
		
	}
	
	public static void scrolltoview(QAFExtendedWebElement element) {
		AppiumDriver driver = (AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);		
	}
	
	
	
	
	public static void jsClick(QAFExtendedWebElement element) {
		AppiumDriver driver = (AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", element);
	}

	
	
	/*public static void scrolltoviewta() {
		AppiumDriver driver = (AppiumDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver();
	//	QAFWebElement e = getDriver().findElement("amazon.addto.cart");
		//List<MobileElement> elementsOne = (List<MobileElement>) driver.findElementsByXPath("//android.widget.Button[@text='Add to Cart']");
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress((QAFExtendedWebElement) elementsOne, 6000).moveTo(1, 1).release().perform();
	}*/
	public static void scrollBy(){
		JavascriptExecutor js = (JavascriptExecutor)getDriver();
		js.executeScript("window.scrollBy(0,250)","");
	}

	/*public  void scroll(double startPt, double endPt) {

		org.openqa.selenium.Dimension dim = (org.openqa.selenium.Dimension) getDriver().manage().window()
				.getSize();
		int screenwidth = (int) (dim.width / 2);
		int scrollingStartPoint = (int) (dim.getHeight() * startPt);
		int scrollingEndPoint = (int) (dim.getHeight() * endPt);
		@SuppressWarnings("all")
		TouchActions touchaction = new TouchActions(getDriver());
		touchaction.press(Point.point(screenwidth, scrollingStartPoint))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(4000)))
				.moveTo(PointOption.point(screenwidth, scrollingEndPoint)).release().perform();
	} 
*/
	
	

	
}
