package com.qmetry.qaf.example.utils;

import org.apache.commons.lang.time.StopWatch;

import com.qmetry.qaf.automation.util.Reporter;

public class Sleeper {

	// For Avoiding the InterruptedException to handle
	// this can be work as pause without any exception

	public static void sleep(long millis) {
		Reporter.log("Sleeping for [ " + millis + " ] milliseconds");
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		do {

		} while (stopwatch.getTime() < millis);
		stopwatch.stop();
		stopwatch.reset();
	}

}
