package com.qmetry.qaf.example.utils;

import java.time.LocalDate;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class DateUtils extends WebDriverBaseTestPage<WebDriverTestPage> {

	public LocalDate selectDate(int selectToDay){
		  LocalDate date = LocalDate.now();  
		     LocalDate seldate=date.plusDays(selectToDay); 
		     return seldate;
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}


}
