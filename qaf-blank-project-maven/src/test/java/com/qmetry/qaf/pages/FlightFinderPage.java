package com.qmetry.qaf.pages;

import java.time.LocalDate;

import org.openqa.selenium.Dimension;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.example.utils.DateUtils;

public class FlightFinderPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "img.findflight.flightfinder")
	private QAFWebElement flightFinder;

	@FindBy(locator = "btn.continue.flightfinder")
	private QAFWebElement btnContinue;

	@FindBy(locator = "rad.roundtrip.flightfinder")
	private QAFWebElement radRoundTrip;

	@FindBy(locator = "rad.oneway.flightfinder")
	private QAFWebElement radOneWay;

	@FindBy(locator = "sel.passengercount.flightfinder")
	private QAFWebElement selPassengernum;

	@FindBy(locator = "sel.departingfrom.flightfinder")
	private QAFWebElement selFromDepart;

	@FindBy(locator = "sel.fromMonth.flightfinder")
	private QAFWebElement selfrommonth;

	@FindBy(locator = "sel.fromDay.flightfinder")
	private QAFWebElement selFromDay;

	@FindBy(locator = "sel.arrivingin.flightfinder")
	private QAFWebElement selArriveport;

	@FindBy(locator = "sel.toMonth.flightfinder")
	private QAFWebElement selToMonth;

	@FindBy(locator = "sel.toDay.flightfinder")
	private QAFWebElement selToDay;

	@FindBy(locator = "rad.economyclass.flightfinder")
	private QAFWebElement radEconomyClass;

	@FindBy(locator = "rad.businessclass.flightfinder")
	private QAFWebElement radBusinessClass;

	@FindBy(locator = "rad.firstclass.flightfinder=name")
	private QAFWebElement radFirstClass;

	@FindBy(locator = "sel.airline.flightfinder")
	private QAFWebElement selAirLine;

	DateUtils dateUtils = new DateUtils();
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}

	public QAFWebElement getFindFlightLogo() {
		return flightFinder;
	}

	public QAFWebElement clkContinue() {
		return btnContinue;
	}

	public QAFWebElement radRoundTripBtn() {
		return radRoundTrip;
	}

	public QAFWebElement radOneWayBtn() {
		return radOneWay;
	}

	public QAFWebElement selPassengerNum() {
		return selPassengernum;
	}

	public QAFWebElement selFromDepart() {
		return selFromDepart;
	}

	public QAFWebElement selfrommonth() {
		return selfrommonth;
	}

	public QAFWebElement selFromDay() {
		return selFromDay;
	}

	public QAFWebElement selArriveport() {
		return selArriveport;
	}

	public QAFWebElement selToMonth() {
		return selToMonth;
	}

	public QAFWebElement selToDay() {
		return selToDay;
	}

	public QAFWebElement radEconomyClassBtn() {
		return radEconomyClass;
	}

	public QAFWebElement radBusinessClassBtn() {
		return radBusinessClass;
	}

	public QAFWebElement radFirstClassBtn() {
		return radFirstClass;
	}

	public QAFWebElement selAirLinePreference() {
		return selAirLine;
	}

	FlightSummaryPage summaryPage = new FlightSummaryPage();

	public void btnContinue() {
		clkContinue().assertEnabled("Validating Button is intractable");
		Reporter.log("Clicking on the Flight Continue Button");
		clkContinue().click();
	}

	public void selPassangerCount(String selectPassangerCount) {
		summaryPage.selectOption(selPassengernum, selectPassangerCount);
	}

	public void selFromDepart(String selectDapartPort) {
		summaryPage.selectOption(selFromDepart, selectDapartPort);
	}

	public void selFromMonth(String selectDapartMonth) {
		summaryPage.selectOption(selfrommonth, selectDapartMonth);
	}

	public void selFromDay(int selectDapartDay) {
		LocalDate selDate = dateUtils.selectDate(selectDapartDay);
		Reporter.log(("Today date: " + selDate));
		summaryPage.selectOption(selToDay, selDate.toString().split("-")[2]);
	}

	public void selArrivePort(String selectArrivePort) {
		summaryPage.selectOption(selArriveport, selectArrivePort);
	}

	public void selArriveMonth(String selectToMonth) {
		summaryPage.selectOption(selToMonth, selectToMonth);
	}

	public void selArriveDay(int selectToDay) {
		LocalDate selDate = dateUtils.selectDate(selectToDay);
		Reporter.log(("Today date: " + selDate));
		summaryPage.selectOption(selToDay, selDate.toString().split("-")[2]);
	}

	public void selAirLinePreference(String selectAirLinePreference) {
		summaryPage.selectOption(selAirLine, selectAirLinePreference);
	}

	public void searchFlightForBooking(String tripType, String selectPassangerCount, String selectDapartPort,
			String selectDapartMonth, int selectDapartDay, String selectArrivePort, String selectToMonth,
			int selectToDay, String serviceClass, String selectAirLinePreference) {
		if (tripType.toLowerCase().contains("roundtrip")) {
			radRoundTripBtn().click();
		} else if (tripType.toLowerCase().contains("oneway")) {
			radOneWayBtn().click();
		} else {
			Reporter.log("Default Trip Type has been Selected as Round Trip");
		}
		selPassangerCount(selectPassangerCount);
		selFromDepart(selectDapartPort);
		if(!selectDapartMonth.equals(" ")){
		selFromMonth(selectDapartMonth);
		}
		else {
			Reporter.log("Default Month has been selected");
		}
		selFromDay(selectDapartDay);
		selArrivePort(selectArrivePort);
		if(!selectToMonth.equals(" ")){
		selArriveMonth(selectToMonth);
		}
		else {
			Reporter.log("Default Month has been selected");
		}
		selArriveDay(selectToDay);

		if (serviceClass.toLowerCase().contains("economy")) {
			radEconomyClassBtn().click();
		} else if (serviceClass.toLowerCase().contains("business")) {
			radBusinessClassBtn().click();
		} else if (serviceClass.toLowerCase().contains("first")) {
			radFirstClassBtn().click();
		} else {
			Reporter.log("Default Trip Type has been Selected as economy");
		}
		selAirLinePreference(selectAirLinePreference);
		btnContinue();

	}
}
