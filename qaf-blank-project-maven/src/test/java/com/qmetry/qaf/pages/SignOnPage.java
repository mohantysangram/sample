package com.qmetry.qaf.pages;



import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SignOnPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="img.signon.signonpage")
	private QAFWebElement imgSignOn;
	
	@FindBy(locator="lnk.signon.signonpage")
	private QAFWebElement lnkSignOn;
	
	@FindBy(locator="txt.welcomeback.signonpage")
	private QAFWebElement txtWelComeBack;
	
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}
	
	public QAFWebElement getImgSignOn() {
		return imgSignOn;
	}

	public QAFWebElement getLnkSignOn() {
		return lnkSignOn;
	}
	public QAFWebElement getTxtWelComeBack() {
		return txtWelComeBack;
	}
	



	
}
