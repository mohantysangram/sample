package com.qmetry.qaf.pages;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegisterInfoBean extends BaseDataBean{
	
	   @Randomizer(length=5,type = RandomizerTypes.LETTERS_ONLY)
	    private String userName,password;
	   @Randomizer(length=5,type = RandomizerTypes.LETTERS_ONLY)
	    private String firstName;
	   @Randomizer(length=5,type = RandomizerTypes.LETTERS_ONLY)
	    private String lastName;
	   
	   @Randomizer(suffix = "ssas", length = 6)
	    private String email;
	   
	    @Randomizer(length=5,type=RandomizerTypes.DIGITS_ONLY)
	    String  postalZipCode;
	    
	    @Randomizer(length=10,type=RandomizerTypes.MIXED)
	    String phoneNum ;
	    
	    @Randomizer(length=20,type = RandomizerTypes.MIXED)
	    private String address;
	    
	    @Randomizer(dataset={"INDIA","ALBANIA","New York","TOGO"})
	    private String country;
	    
	    @Randomizer(dataset={"Bangalore","Pune","Odisha","Tamilnadu"})
	    private String city;
	    
	    public String getCityName(){
	    	return city;
	    }
	    
	    public String getFirstName(){
	    	return firstName;
	    }
	    
	    public String getLastName(){
	    	return lastName;
	    }
	    
	    public String getUserName(){
	    	return userName;
	    }
	     
	    public String getPassword(){
	    	return password;
	    }
	    
	    public String getEmail(){
	    	return email;
	    }
	    
	    public String getPostalZipCode(){
	    	return postalZipCode;
	    }
	    
	    public String getPhoneNum(){
	    	return phoneNum;
	    }
	    
	    public String getAddress(){
	    	return address;
	    }
	    
	    public String getCountry(){
	    	return country;
	    }

}
