package com.qmetry.qaf.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class SelectFlightPage  extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator="radio.selectflight.selectflightpage")
	private QAFWebElement selFlight;
	
	@FindBy(locator="button.reserveflight.selectflightpage")
	private QAFWebElement revFlight;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}
	
	public QAFWebElement selectFight() {
		return selFlight;
	}
	public QAFWebElement revFlight() {
		return revFlight;
	}
	
	public void btnReserveFlight(){
		Reporter.log("Clicking on the Continue button for Flight Reservation");
		revFlight.click();
	}
	
	public void selectFlight(String flightjourneyType,String flightName){
		QAFExtendedWebElement flightType=new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("radio.selectflight.selectflightpage"),flightjourneyType,flightName));
		System.out.println("xpath Is "+flightType);
		flightType.click();
	}
	
	public void getFlightName(String arriveLocationName,String departLocationName){
		QAFExtendedWebElement flightName=new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("txt.flightname.selectflightpage"),arriveLocationName,departLocationName));
		System.out.println("xpath Is "+flightName);
		flightName.verifyPresent("Arrive Location Name :"+arriveLocationName+" And Depart Location Name :"+departLocationName);
	}
	
}
