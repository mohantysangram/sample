package com.qmetry.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class RegisterPage  extends WebDriverBaseTestPage<WebDriverTestPage>{

	
	@FindBy(locator = "txt.firstname.registerpage")
	private QAFWebElement firstName;
	
	@FindBy(locator = "txt.lastname.registerpage")
	private QAFWebElement lastName;
	
	@FindBy(locator = "txt.phonenum.registerpage")
	private QAFWebElement phoneNum;
	
	@FindBy(locator = "txt.mailid.registerpage")
	private QAFWebElement mailId;
	
	@FindBy(locator = "txt.addressone.registerpage")
	private QAFWebElement addressOne;
	
	@FindBy(locator = "txt.addresstwo.registerpage")
	private QAFWebElement addressTwo;
	
	@FindBy(locator = "txt.city.registerpage")
	private QAFWebElement cityName;
	
	@FindBy(locator = "txt.state.registerpage")
	private QAFWebElement stateName;
	
	@FindBy(locator = "txt.postalCode.registerpage")
	private QAFWebElement postalCode;
	
	@FindBy(locator = "txt.country.registerpage")
	private QAFWebElement countyName;
	
	@FindBy(locator = "txt.username.registerpage")
	private QAFWebElement userName;
	
	@FindBy(locator = "txt.password.registerpage")
	private QAFWebElement password;
	
	@FindBy(locator = "txt.confirmPassword.registerpage")
	private QAFWebElement confPassword;
	
	@FindBy(locator = "btn.register.registerpage")
	private QAFWebElement btnRegister;

	@FindBy(locator = "txt.message.registerpage")
	private QAFWebElement dearRegisterUserMsg;
	
	@FindBy(locator = "txt.successmessage.registerpage")
	private QAFWebElement successRegisterMsg;
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}
	
	
	
	
	public QAFWebElement getDearRegisterUserMsg() {
		return dearRegisterUserMsg;
	}
	public QAFWebElement getSuccessRegisterMsg() {
		return successRegisterMsg;
	}
	public QAFWebElement getfirstName() {
		return firstName;
	}
	
	public QAFWebElement getLastName() {
		return lastName;
	}
	
	public QAFWebElement getPhoneNum() {
		return phoneNum;
	}
	
	public QAFWebElement getMailId() {
		return mailId;
	}
	
	
	public QAFWebElement getaddressOne() {
		return addressOne;
	}
	
	public QAFWebElement getaddressTwo() {
		return addressTwo;
	}
	
	public QAFWebElement getCityName() {
		return cityName;
	}
	
	public QAFWebElement getstateName() {
		return stateName;
	}
	
	public QAFWebElement getPostalCode() {
		return postalCode;
	}
	
	public QAFWebElement getCountyName() {
		return countyName;
	}
	
	public QAFWebElement getUserName() {
		return userName;
	}
	
	public QAFWebElement getPassword() {
		return password;
	}
	
	public QAFWebElement getConfPassword() {
		return confPassword;
	}
	
	public QAFWebElement btnSubmit() {
		return btnRegister;
	}

	
	public Object registerUser(){
		
		HomePage homePage=new HomePage();
		homePage.getRegisterLink().click();
		
		RegisterInfoBean infoBean=new RegisterInfoBean();
		infoBean.fillFromConfig("data.RegisterTest.user");
		getfirstName().sendKeys(infoBean.getFirstName());
		getLastName().sendKeys(infoBean.getLastName() 	);
		getPhoneNum().sendKeys(infoBean.getPhoneNum());
		getMailId().sendKeys(infoBean.getEmail());
		getaddressOne().sendKeys(infoBean.getAddress());
		getaddressTwo().sendKeys(infoBean.getAddress());
		getCityName().sendKeys(infoBean.getCityName());
		getstateName().sendKeys(infoBean.getCityName());
		getPostalCode().sendKeys(infoBean.getPostalZipCode());
		getCountyName().sendKeys(infoBean.getCountry());
		getUserName().sendKeys(infoBean.getUserName());
		getPassword().sendKeys(infoBean.getPassword());
		getConfPassword().sendKeys(infoBean.getPassword());
		btnSubmit().click();
		return infoBean;
	}
	
	public void registerWithRandomValue(){
		HomePage homePage=new HomePage();
		homePage.getRegisterLink().click();
		RegisterInfoBean infoBean=new RegisterInfoBean();
		infoBean.fillRandomData();
		
		//infoBean.fillFromConfig("data.RegisterTest.user");
		getfirstName().sendKeys(infoBean.getFirstName());
		getLastName().sendKeys(infoBean.getLastName() 	);
		getPhoneNum().sendKeys(infoBean.getPhoneNum());
		getMailId().sendKeys(infoBean.getEmail());
		getaddressOne().sendKeys(infoBean.getAddress());
		getaddressTwo().sendKeys(infoBean.getAddress());
		getCityName().sendKeys(infoBean.getCityName());
		getstateName().sendKeys(infoBean.getCityName());
		getPostalCode().sendKeys(infoBean.getPostalZipCode());
		getCountyName().sendKeys(infoBean.getCountry());
		getUserName().sendKeys(infoBean.getUserName());
		getPassword().sendKeys(infoBean.getPassword());
		getConfPassword().sendKeys(infoBean.getPassword());
		btnSubmit().click();
	}
	
}

