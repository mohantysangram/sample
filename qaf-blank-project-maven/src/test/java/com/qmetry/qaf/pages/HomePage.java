package com.qmetry.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "img.applogo.homepage")
	private QAFWebElement homePageLogo;

	@FindBy(locator = "txt.username.homepage")
	private QAFWebElement txtUserName;

	@FindBy(locator = "txt.password.homepage")
	private QAFWebElement txtPassword;

	@FindBy(locator = "btn.signIn.homepage")
	private QAFWebElement btnSignIn;

	@FindBy(locator = "lnk.signIn.homepage")
	private QAFWebElement lnkSignIn;

	@FindBy(locator = "lnk.register.homepage")
	private QAFWebElement lnkRegister;

	@FindBy(locator = "lnk.support.homepage")
	private QAFWebElement lnksupport;

	@FindBy(locator = "lnk.contact.homepage")
	private QAFWebElement lnkContact;

	@FindBy(locator = "lnk.home.homepage")
	private QAFWebElement lnkHome;

	@FindBy(locator = "lnk.flights.homepage")
	private QAFWebElement lnkFlights;

	@FindBy(locator = "lnk.hotels.homepage")
	private QAFWebElement lnkHotels;

	@FindBy(locator = "lnk.carRentals.homepage")
	private QAFWebElement lnkCarRentals;

	@FindBy(locator = "lnk.cruises.homepage")
	private QAFWebElement lnkCruises;

	@FindBy(locator = "lnk.destinations.homepage")
	private QAFWebElement lnkDestination;

	@FindBy(locator = "lnk.vacations.homepage")
	private QAFWebElement lnkVacations;

	public QAFWebElement getHomePageLogo() {
		return homePageLogo;
	}

	public QAFWebElement getUserName() {
		return txtUserName;
	}

	public QAFWebElement getPassword() {
		return txtPassword;
	}

	public QAFWebElement getSignInLink() {
		return lnkSignIn;
	}

	public QAFWebElement getRegisterLink() {
		return lnkRegister;
	}

	public QAFWebElement getSupportLink() {
		return lnksupport;
	}

	public QAFWebElement getContactLink() {
		return lnkContact;
	}

	public QAFWebElement getHomeLink() {
		return lnkHome;
	}

	public QAFWebElement getFlightsLink() {
		return lnkFlights;
	}

	public QAFWebElement getHotelLink() {
		return lnkHotels;
	}

	public QAFWebElement getCarRentalsLink() {
		return lnkCarRentals;
	}

	public QAFWebElement getCruisesLink() {
		return lnkCruises;
	}

	public QAFWebElement getDestinationLink() {
		return lnkDestination;
	}

	public QAFWebElement getVacationLink() {
		return lnkVacations;
	}

	public QAFWebElement btnSignIn() {
		return btnSignIn;
	}

	public void SignIn(String userName, String password) {
		Reporter.log("Clearing the user name field and entering the UserName as :" + userName);
		getUserName().sendKeys(userName);
		Reporter.log("Clearing the Password field and entering the Password ");
		getPassword().sendKeys(password);
		btnSignIn().assertEnabled("Validating Button is intractable");
		Reporter.log("Clicking on the Sign In Button");
		btnSignIn().click();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}

	public void verfiyfotterlink() {
		getHomePageLogo().verifyVisible();

	}

	public void btnContact() {
		getSupportLink().click();
	}

	public void getRegisterlink() {
		getRegisterLink().click();
	}
}
