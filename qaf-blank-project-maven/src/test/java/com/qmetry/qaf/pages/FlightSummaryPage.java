package com.qmetry.qaf.pages;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightSummaryPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	/** --------Passenger Details----------- **/
	@FindBy(locator = "txt.passengersfirstname.flightsummarypage")
	private QAFWebElement txtpassengerFirstName;
	@FindBy(locator = "txt.passengerslastname.flightsummarypage")
	private QAFWebElement txtpassengerLastName;
	@FindBy(locator = "sel.passengersmeal.flightsummarypage")
	private QAFWebElement selpassengerMeals;

	/** --------Credit Card Details--------- **/
	@FindBy(locator = "sel.cctype.flightsummarypage")
	private QAFWebElement selpassengerCCType;
	@FindBy(locator = "txt.ccnumber.flightsummarypage")
	private QAFWebElement txtpassengerCCNumber;
	@FindBy(locator = "sel.ccexpmonth.flightsummarypage")
	private QAFWebElement selpassengerCCExpMonth;
	@FindBy(locator = "sel.ccexpyear.flightsummarypage")
	private QAFWebElement selpassengerCCExpYear;
	@FindBy(locator = "txt.ccfirstname.flightsummarypage")
	private QAFWebElement txtpassengerCCFirstName;
	@FindBy(locator = "txt.ccmidname.flightsummarypage")
	private QAFWebElement txtpassengerCCMidName;
	@FindBy(locator = "txt.cclastname.flightsummarypage")
	private QAFWebElement txtpassengerCCLastName;

	/** --------Billing Details--------- **/
	@FindBy(locator = "txt.billingaddressone.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressone;
	@FindBy(locator = "txt.billingaddrestwo.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressTwo;
	@FindBy(locator = "txt.billingaddresscity.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressCity;
	@FindBy(locator = "txt.billingaddressstate.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressState;
	@FindBy(locator = "txt.billingaddresszip.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressZipCode;
	@FindBy(locator = "sel.billingaddresscountry.flightsummarypage")
	private QAFWebElement txtpassengerBillingAddressCountry;

	/** --------Delivery Details--------- **/
	@FindBy(locator = "txt.deliveryaddresstwo.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressone;
	@FindBy(locator = "txt.deliveryaddressone.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressTwo;
	@FindBy(locator = "txt.deliveryaddresscity.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressCity;
	@FindBy(locator = "txt.deliveryaddressstate.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressState;
	@FindBy(locator = "txt.deliveryaddresszip.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressZipCode;
	@FindBy(locator = "sel.deliveryaddresscountry.flightsummarypage")
	private QAFWebElement txtpassengerDeliveryAddressCountry;

	@FindBy(locator = "txt.buyflight.flightsummarypage")
	private QAFWebElement btnBuyFlight;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}

	public QAFWebElement enterPassengerFirstName() {
		return txtpassengerFirstName;
	}

	public QAFWebElement enterPassengerLastName() {
		return txtpassengerLastName;
	}

	public QAFWebElement selectPassengerMeal() {
		return selpassengerMeals;
	}

	public QAFWebElement selectPassengerCCType() {
		return selpassengerCCType;
	}

	public QAFWebElement enterPassengerCCNumber() {
		return txtpassengerCCNumber;
	}

	public QAFWebElement selectPassengerCCExpMonth() {
		return selpassengerCCExpMonth;
	}

	public QAFWebElement selectPassengerCCExpYear() {
		return selpassengerCCExpYear;
	}

	public QAFWebElement enterPassengerCCFirstName() {
		return txtpassengerCCFirstName;
	}

	public QAFWebElement enterPassengerCCMidName() {
		return txtpassengerCCMidName;
	}

	public QAFWebElement enterPassengerCCLastName() {
		return txtpassengerCCLastName;
	}

	public QAFWebElement enterPassengerBillingAddressOne() {
		return txtpassengerBillingAddressone;
	}

	public QAFWebElement enterPassengerBillingAddressTwo() {
		return txtpassengerBillingAddressTwo;
	}

	public QAFWebElement enterPassengerBillingAddressCity() {
		return txtpassengerBillingAddressCity;
	}

	public QAFWebElement enterPassengerBillingAddressState() {
		return txtpassengerBillingAddressState;
	}

	public QAFWebElement enterPassengerBillingAddressZipCode() {
		return txtpassengerBillingAddressZipCode;
	}

	public QAFWebElement selectPassengerBillingAddressCountry() {
		return txtpassengerBillingAddressCountry;
	}

	public QAFWebElement enterPassengerDeliveryAddressOne() {
		return txtpassengerDeliveryAddressone;
	}

	public QAFWebElement enterPassengerDeliveryAddressTwo() {
		return txtpassengerDeliveryAddressTwo;
	}

	public QAFWebElement enterPassengerDeliveryAddressCity() {
		return txtpassengerDeliveryAddressCity;
	}

	public QAFWebElement enterPassengerDeliveryAddressState() {
		return txtpassengerDeliveryAddressState;
	}

	public QAFWebElement enterPassengerDeliveryAddressZipCode() {
		return txtpassengerDeliveryAddressZipCode;
	}

	public QAFWebElement selectPassengerDeliveryAddressCountry() {
		return txtpassengerDeliveryAddressCountry;
	}

	public QAFWebElement clkBuyFlight() {
		return btnBuyFlight;
	}

	public List<WebElement> selectOption(QAFWebElement webElement, String selectDesiredOption) {
		Select selectDDOption = new Select(webElement);
		selectDDOption.selectByVisibleText(selectDesiredOption);
		List<WebElement> allOptions = selectDDOption.getOptions();
		return allOptions;

	}

	public void passengersDetails(String passengerFirstName, String passengerLastName, String mealType) {
		if (!passengerFirstName.equals("")) {
			enterPassengerFirstName().sendKeys(passengerFirstName);
		} else {
			Reporter.log("Please Eneter the Passenger First Name");
		}
		if (!passengerFirstName.equals("")) {
			enterPassengerLastName().sendKeys(passengerLastName);
		} else {
			Reporter.log("Please Eneter the Passenger Last Name");
		}
		selectOption(selectPassengerMeal(), mealType);
	}

	public void credirCardDetails(String passengerCCType, String passengerCCNumber, String passengerCCExpMonth,
			String passengerCCExpYear, String passengerCCFirstName, String passengerCCMidName,
			String passengerCCLastName) {
		selectOption(selectPassengerCCType(), passengerCCType);
		enterPassengerCCNumber().sendKeys(passengerCCNumber);
		if (!passengerCCExpMonth.equals(" ")) {
			selectOption(selectPassengerCCExpMonth(), passengerCCExpMonth);
		}
		
		try {
			if (!passengerCCExpYear.equals(" ")) {
					selectOption(selectPassengerCCExpYear(), passengerCCExpYear);
			}
		} catch (NoSuchElementException e) {
		Assert.fail("Passed Year is not present on UI,Please select a Year from the available list");
		throw e;
		} 
		enterPassengerCCFirstName().sendKeys(passengerCCFirstName);
		if (!passengerCCMidName.equals("")) {
			enterPassengerCCMidName().sendKeys(passengerCCMidName);
		}
		enterPassengerCCLastName().sendKeys(passengerCCLastName);
	}

	public void billingAddressDetails(String billingAddressOne, String billingAddressTwo, String billingAddressCity,
			String billingAddressState, String billingAddressZipCode, String billingAddressCountry) {
		enterPassengerBillingAddressOne().sendKeys(billingAddressOne);
		enterPassengerBillingAddressTwo().sendKeys(billingAddressTwo);
		enterPassengerBillingAddressCity().sendKeys(billingAddressCity);
		enterPassengerBillingAddressState().clear();
		enterPassengerBillingAddressState().sendKeys(billingAddressState);
		enterPassengerBillingAddressZipCode().clear();
		enterPassengerBillingAddressZipCode().sendKeys(billingAddressZipCode);
		this.selectOption(selectPassengerBillingAddressCountry(), billingAddressCountry);
	}

	public void deliveryAddressDetails(String deliveryAddressOne, String deliveryAddressTwo, String deliveryAddressCity,
			String deliveryAddressState, String deliveryAddressZipCode, String deliveryAddressCountry) {
		enterPassengerDeliveryAddressOne().sendKeys(deliveryAddressOne);
		enterPassengerDeliveryAddressTwo().sendKeys(deliveryAddressTwo);
		enterPassengerDeliveryAddressCity().sendKeys(deliveryAddressCity);
		enterPassengerDeliveryAddressState().clear();
		enterPassengerDeliveryAddressState().sendKeys(deliveryAddressState);
		enterPassengerDeliveryAddressZipCode().clear();
		enterPassengerDeliveryAddressZipCode().sendKeys(deliveryAddressZipCode);
		this.selectOption(selectPassengerDeliveryAddressCountry(), deliveryAddressCountry);
	}

	public String alertHandler() {
		String flag;
		flag = driver.switchTo().alert().getText();
		if (!flag.equals("")) {
			driver.switchTo().alert().accept();
		}
		return flag;

	}

	public void btnBookFlight() {
		Reporter.log("Clicking on the Book Flight Button");
		clkBuyFlight().click();
	}

	public String getFlightDetails(String selectDapartPort, String selectArrivePort) {
		QAFExtendedWebElement flightType = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("txt.getflight.flightsummarypage"),
						selectDapartPort, selectArrivePort));
		System.out.println("flight name Is " + flightType.getText());
		return flightType.getText();
	}
}
