package com.qmetry.qaf.pages;



import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightConfirmationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "img.flightconfirmation.flightconfirmationpage")
	private QAFWebElement txtFlightConfirmation;

	@FindBy(locator = "txt.itinerary.flightconfirmationpage")
	private QAFWebElement txtFlightitineraryConfirmation;

	@FindBy(locator = "btn.logout.flightconfirmationpage")
	private QAFWebElement btnLogOut;

	@FindBy(locator = "btn.backtohome.flightconfirmationpage")
	private QAFWebElement btnBackToHome;

	@FindBy(locator = "btn.backtoflight.flightconfirmationpage")
	private QAFWebElement btnBackToFlight;

	@FindBy(locator = "lnk.signoff.flightconfirmationpage")
	private QAFWebElement lnkSignOff;
	
	@FindBy(locator = "txt.passengername.flightconfirmationpage")
	private QAFWebElement txtPassengerName;
	
	@FindBy(locator = "txt.flightdetails.flightconfirmationpage")
	private List<QAFWebElement> txtFlightdetails;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}

	public QAFWebElement getFlightName() {
		return txtFlightConfirmation;
	}
	public QAFWebElement getPassengerName() {
		return txtPassengerName;
	}
	
	public List<QAFWebElement> getFlightdetails() {
		return txtFlightdetails;
	}

	public QAFWebElement getFlightItineraryConfirmation() {
		return txtFlightitineraryConfirmation;
	}

	public QAFWebElement logOutBtn() {
		return btnLogOut;
	}

	public QAFWebElement backToHomebtn() {
		return btnBackToHome;
	}

	public QAFWebElement backToFlightbtn() {
		return btnBackToFlight;
	}

	public QAFWebElement signOffLink() {
		return lnkSignOff;
	}

	public void clkLogOutBtn() {
		logOutBtn().click();
	}

	public void clkBackToHomeBtn() {
		Reporter.log("Clicking on the Back To Home Button in Flight Confirmation Page");
		backToHomebtn().click();
	}

	public void clkBackToFlightBtn() {
		Reporter.log("Clicking on the Back To Flight Button in Flight Confirmation Page");
		backToFlightbtn().click();
	}

	public void clkSignOffLink() {
		Reporter.log("Clicking on Sign Off Link in Flight Confirmation Page");
		signOffLink().click();
	}

	public void jsClick(QAFWebElement qafWebElement) {

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", qafWebElement);
	}
	
	public void verifyDepartingDetails() {
		List<QAFWebElement> flightDetails=getFlightdetails();
		for(QAFWebElement flightListData :flightDetails ){
			System.out.println("Flight Details are :"+flightListData.getText());
			
		}

	}

	public void verifyReturningDetails() {

	}

	public void verifyPassengerCount() {

	}
	public String verifyBillingTodetails(String firstName,String lastName) {
		QAFExtendedWebElement passengerName=new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("txt.passengername.flightconfirmationpage"),firstName,lastName));
		System.out.println("xpath Is "+passengerName);
		ConfigurationManager.getBundle().clear();
		return passengerName.getText();
	}

	public void verifypaymentDetails() {

	}

}
