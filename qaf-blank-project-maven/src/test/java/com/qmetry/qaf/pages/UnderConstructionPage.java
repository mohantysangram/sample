package com.qmetry.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class UnderConstructionPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "txt.messsage.underconstructionpage")
	private QAFWebElement txtInconvieneceMsg;
	
	@FindBy(locator = "btn.backtohome.underconstructionpage")
	private QAFWebElement btnBackToHome;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.manage().window().maximize();
		driver.get("/");
	}
	
	public QAFWebElement getTxtInconvieneceMsg() {
		return txtInconvieneceMsg;
	}
	
	public QAFWebElement getbtnBackToHome() {
		return btnBackToHome;
	}
	
	public void btnBackToHome(){
		getbtnBackToHome().click();
	}
}
