package com.qmetry.qaf.tests;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.RegisterPage;
import com.qmetry.qaf.pages.SignOnPage;

public class LoginPageTest extends WebDriverTestCase {
	
	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "LoginTest")
	public void loginTest(Map<String,String>data) {
		
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		
		homePage.SignIn(data.get("userName"),data.get("password") + 123);
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");
		
		SignOnPage signOnPage=new SignOnPage();
		signOnPage.getImgSignOn().verifyPresent("Sign On Image Is Displayed");
		signOnPage.getLnkSignOn().verifyPresent("Sign On Link Is Displayed");
		signOnPage.getTxtWelComeBack().verifyPresent("In Sign On Page Welcome back to Mercury Tours! Is Displayed");
		
		homePage.SignIn(data.get("userName") + 123, data.get("password"));
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");
		
		signOnPage.getImgSignOn().verifyPresent("Sign On Image Is Displayed");
		signOnPage.getLnkSignOn().verifyPresent("Sign On Link Is Displayed");
		signOnPage.getTxtWelComeBack().verifyPresent("In Sign On Page Welcome back to Mercury Tours! Is Displayed");
		
		
		homePage.SignIn(data.get("userName"), data.get("password"));
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");
		
		FlightFinderPage finderPage=new FlightFinderPage();
		finderPage.getFindFlightLogo().verifyVisible("Flight Finder Logo Is visible After Logged In");
	}
	
	@Test(description = "RegisterTest")
	public void registerTest() {

		HomePage homePage=new HomePage();
		homePage.launchPage(null);
		
		RegisterPage registerPage=new RegisterPage();
		Object beanData= registerPage.registerUser();
		Map <String,Object> hashMap = new HashMap <String,Object> ();
		hashMap.put("users", beanData);
		Reporter.log("Map Values are "+hashMap);
		for(Entry<String, Object> e:hashMap.entrySet()){
			Reporter.log(e.getKey() + "is Present "+ e.getValue());
			String Name=e.getValue().equals("firstName")+"";
			Reporter.log("First  Name Is "+Name);
		}
		
		String registerUserName=registerPage.getDearRegisterUserMsg().getText();
		Reporter.log("User Name Is "+registerUserName);
		String successRegisterUserName=registerPage.getSuccessRegisterMsg().getText();
		Reporter.log("Success Message "+successRegisterUserName);
		Validator.verifyThat("Vlaidating registered name", registerUserName, Matchers.equalToIgnoringCase("Dear Elena Gilbert,"));
		Validator.verifyThat("Vlaidating Success Message", successRegisterUserName, Matchers.containsString("Thank you for registering."));
		Reporter.log("Last  Name Is "+hashMap.get("lastName"));
		
		
	}
	
	@Test(description = "InvalidUserDataTest")
	public void InvalidUserDataTest() {

		HomePage homePage=new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		
		RegisterPage registerPage=new RegisterPage();
		registerPage.registerWithRandomValue();
		String successRegisterUserName=registerPage.getSuccessRegisterMsg().getText();
		Validator.verifyThat("Vlaidating Success Message", successRegisterUserName, Matchers.containsString("Thank you for registering."));
	
		
	}
	

}
