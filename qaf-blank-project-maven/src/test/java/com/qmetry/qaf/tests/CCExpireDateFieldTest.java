package com.qmetry.qaf.tests;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.pages.FlightConfirmationPage;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.FlightSummaryPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class CCExpireDateFieldTest extends WebDriverTestCase {

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "ValidateExpireDateFieldTest")
	public void validateExpireDateFieldTest(Map<String, String> data) {

		Reporter.log("Validating the Expiry date field");
		HomePage homePage = new HomePage();
		homePage.launchPage(null);
		homePage.SignIn(data.get("userName"), data.get("password"));
		
		String departFlightName=null;
		String arriveFlightName =null;
		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		FlightFinderPage finderPage = new FlightFinderPage();
		SelectFlightPage flightPage = new SelectFlightPage();
		FlightSummaryPage summaryPage = new FlightSummaryPage();
		
		
		Reporter.log("Validating the Expiry date as Empty");
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		departFlightName = summaryPage.getFlightDetails(data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Validating depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"), data.get("selectDapartPort"));
		Validator.verifyThat("Validating Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		
		
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				" ", " ", data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.clkBuyFlight();
		confirmationPage.verifyDepartingDetails();

		
		Reporter.log("Validating Expiry Date As Past Date");
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		departFlightName = summaryPage.getFlightDetails(data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Validating depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"), data.get("selectDapartPort"));
		Validator.verifyThat("Validating Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"),"2000", data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.clkBuyFlight();
		confirmationPage.verifyDepartingDetails();


		Reporter.log("Validating Present Expiry Date");
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		departFlightName = summaryPage.getFlightDetails(data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Validating depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"), data.get("selectDapartPort"));
		Validator.verifyThat("Validating Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		String[] practice=new String[5];
		
		
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), "2019", data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.clkBuyFlight();
		confirmationPage.verifyDepartingDetails();
	}
}
