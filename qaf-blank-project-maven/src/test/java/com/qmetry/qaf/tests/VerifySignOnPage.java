package com.qmetry.qaf.tests;

import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.pages.FlightConfirmationPage;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.FlightSummaryPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class VerifySignOnPage extends WebDriverTestCase {

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifySignOnPageShouldBeDisplayedAfterClickingOnBackToFlightFromFlightConfirmationPage")
	public void verifySignOnPageShouldBeDisplayedAfterClickingOnBackToFlightFromFlightConfirmationPage(
			Map<String, String> data) {

		HomePage homePage = new HomePage();
		homePage.launchPage(null);

		homePage.SignIn(data.get("userName"), data.get("password"));
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");

		FlightFinderPage finderPage = new FlightFinderPage();
		finderPage.getFindFlightLogo().verifyVisible("Flight Finder Logo Is visible After Logged In");
		finderPage.btnContinue();
		
		SelectFlightPage selectFlightPage = new SelectFlightPage();
		selectFlightPage.btnReserveFlight();

		FlightSummaryPage flightSummaryPage = new FlightSummaryPage();
		flightSummaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		flightSummaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"),
				data.get("passengerCCMidName"), data.get("passengerCCLastName"));
		flightSummaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		flightSummaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		String popUpText = flightSummaryPage.alertHandler();
		Validator.verifyThat("validating the Pop Up Message", popUpText, Matchers.containsString(
				"You have chosen a mailing location outside of the United States and its territories. An additional charge of $6.5 will be added as mailing charge."));

		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		confirmationPage.jsClick(flightSummaryPage.clkBuyFlight());
		confirmationPage.waitForPageToLoad();
		confirmationPage.signOffLink().verifyVisible("Select sign Off Link is Displayed");
		confirmationPage.clkLogOutBtn();
		finderPage.clkContinue().verifyVisible("Select Flight find is Displayed");
	}

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifySignOnPageShouldBeDisplayedAfterLogoutFromFlightConfirmationPage")
	public void verifySignOnPageShouldBeDisplayedAfterLogoutFromFlightConfirmationPage(Map<String, String> data) {

		System.err.println(data.toString());
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);

		homePage.SignIn(data.get("userName"), data.get("password"));
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");

		FlightFinderPage finderPage = new FlightFinderPage();
		finderPage.getFindFlightLogo().verifyVisible("Flight Finder Logo Is visible After Logged In");

		finderPage.btnContinue();
		SelectFlightPage selectFlightPage = new SelectFlightPage();
		selectFlightPage.btnReserveFlight();

		FlightSummaryPage flightSummaryPage = new FlightSummaryPage();
		flightSummaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		flightSummaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"),
				data.get("passengerCCMidName"), data.get("passengerCCLastName"));
		flightSummaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		flightSummaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		String popUpText = flightSummaryPage.alertHandler();
		Validator.verifyThat("validating the Pop Up Message", popUpText, Matchers.containsString(
				"You have chosen a mailing location outside of the United States and its territories. An additional charge of $6.5 will be added as mailing charge."));
		
		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		flightSummaryPage.clkBuyFlight();
		
		confirmationPage.waitForPageToLoad();
		confirmationPage.signOffLink().verifyVisible("Sign offLink Is Displayed");
		confirmationPage.clkLogOutBtn();
		homePage.getSignInLink().verifyVisible("Sign On Page Is Displayed");
	}

}
