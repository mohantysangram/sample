package com.qmetry.qaf.tests;

import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.RandomStringGenerator;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.pages.FlightConfirmationPage;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.FlightSummaryPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class CCNumberFieldTest extends WebDriverTestCase {

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "ValidateCCNumberFieldTest")
	public void validateCCNumberFieldTest(Map<String, String> data) {

		Reporter.log("Validating the MOre Than 16 Digit for the CC Number");
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		homePage.SignIn(data.get("userName"), data.get("password"));
		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		FlightFinderPage finderPage = new FlightFinderPage();
		SelectFlightPage flightPage = new SelectFlightPage();
		FlightSummaryPage summaryPage = new FlightSummaryPage();

	
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		
		String departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		String arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		
		String creditCardNum = RandomStringGenerator.get(20, RandomizerTypes.DIGITS_ONLY);
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.enterPassengerCCNumber().clear();
		summaryPage.enterPassengerCCNumber().sendKeys(creditCardNum);
		String modifiedCCNum = summaryPage.enterPassengerCCNumber().getText();
		Validator.verifyTrue(!creditCardNum.equals(modifiedCCNum),
				"Both the CC number matched but it should not matched", "both the CC Card Number did not matched");

		// Validating the Alphanumeric for the CC Number
		Reporter.log("Validating the Alphanumeric for the CC Number");

		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		departFlightName = summaryPage.getFlightDetails(data.get("selectDapartPort"), data.get("selectArrivePort"));
		
		 departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		 arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		
		creditCardNum = RandomStringGenerator.get(20, RandomizerTypes.MIXED);
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.enterPassengerCCNumber().clear();
		summaryPage.enterPassengerCCNumber().sendKeys(creditCardNum);

		
		confirmationPage.jsClick(summaryPage.clkBuyFlight());
		confirmationPage.verifyDepartingDetails();

		// Validating the less than 16 Digit for the CC Number
		Reporter.log("Validating the less than 16 Digit for the CC Number");
		homePage.getFlightsLink().click();

		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		
		 departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		 arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		
		creditCardNum = RandomStringGenerator.get(12, RandomizerTypes.DIGITS_ONLY);
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.enterPassengerCCNumber().clear();
		summaryPage.enterPassengerCCNumber().sendKeys(creditCardNum);
		summaryPage.clkBuyFlight();
		confirmationPage.verifyDepartingDetails();

		// Validating the Only 16 Digit for the CC Number
		Reporter.log("Validating the Only 16 Digit for the CC Number");

		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();
		
		 departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		creditCardNum = RandomStringGenerator.get(16, RandomizerTypes.DIGITS_ONLY);
		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		summaryPage.enterPassengerCCNumber().clear();
		summaryPage.enterPassengerCCNumber().sendKeys(creditCardNum);
		summaryPage.clkBuyFlight();
		confirmationPage.verifyDepartingDetails();

	}

}
