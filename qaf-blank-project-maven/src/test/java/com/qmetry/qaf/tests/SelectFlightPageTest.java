package com.qmetry.qaf.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class SelectFlightPageTest extends WebDriverTestCase {

	@QAFDataProvider(key = "VerifyUsersWithDifferentDepartureAndArrivalDatesShouldBeAbleToMoveToSelectFlightPage.user")
	@Test(description = "VerifyUserAbleToNavigateToSelectFlightPageAfterGivingSameDepartAndArriveLocation")
	public void verifyUserAbleToNavigateToSelectFlightPageAfterGivingSameDepartAndArriveLocation(
			Map<String, String> data) {

		System.err.println(data.toString());
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);

		homePage.SignIn(data.get("userName"), data.get("password"));
		FlightFinderPage finderPage = new FlightFinderPage();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"),
				data.get("selectDapartPort"), data.get("selectDapartMonth"),
				Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"),
				data.get("selectarriveMonth"), Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"),
				data.get("selectAirLinePreference"));

		SelectFlightPage flightPage = new SelectFlightPage();
		flightPage.selectFlight("DEPART", data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.revFlight().verifyPresent("Reservation Button Is displayed on select Flight Page");
		flightPage.getFlightName(data.get("selectDapartPort"), data.get("selectArrivePort"));
	}

	@QAFDataProvider(key = "VerifyUsersWithDifferentDepartureAndArrivalDatesShouldBeAbleToMoveToSelectFlightPage.user")
	@Test(description = "VerifyUsersWithDifferentDepartureAndArrivalDatesShouldBeAbleToMoveToSelectFlightPage")
	public void verifyUsersWithDifferentDepartureAndArrivalDatesShouldBeAbleToMoveToSelectFlightPage(
			Map<String, String> data) {

		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);

		homePage.SignIn(data.get("userName"), data.get("password"));
		FlightFinderPage finderPage = new FlightFinderPage();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"),
				data.get("selectDapartPort"), data.get("selectDapartMonth"),
				Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"),
				data.get("selectarriveMonth"), Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"),
				data.get("selectAirLinePreference"));

		SelectFlightPage flightPage = new SelectFlightPage();
		flightPage.selectFlight("DEPART", data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.revFlight().verifyPresent("Reservation Button Is displayed on select Flight Page");

		Reporter.log("After selecting past depart month :" + data.get("selectDapartMonth") + "Arriving month :"
				+ data.get("selectarriveMonth") + "User Shold be able to navigate to select Flight Page");
		flightPage.getFlightName(data.get("selectDapartPort"), data.get("selectArrivePort"));

		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"),
				data.get("selectDapartPort"), "April", Integer.parseInt(data.get("selectDapartDay")),
				data.get("selectArrivePort"), data.get("selectarriveMonth"), Integer.parseInt(data.get("selectToDay")),
				data.get("serviceClass"), data.get("selectAirLinePreference"));

		flightPage.selectFlight("DEPART", data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.revFlight().verifyPresent("Reservation Button Is displayed on select Flight Page");

		Reporter.log("After selecting Future depart month :April Past Arriving month :" + data.get("selectDapartMonth")
				+ "User Shold be able to navigate to select Flight Page");
		flightPage.getFlightName(data.get("selectDapartPort"), data.get("selectArrivePort"));

		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"),
				data.get("selectDapartPort"), data.get("selectDapartMonth"),
				Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), "April",
				Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"),
				data.get("selectAirLinePreference"));

		flightPage.selectFlight("DEPART", data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.revFlight().verifyPresent("Reservation Button Is displayed on select Flight Page");

		Reporter.log("After selecting past depart month :" + data.get("selectDapartMonth")
				+ "Future Arriving month : April User Shold be able to navigate to select Flight Page");
		flightPage.getFlightName(data.get("selectDapartPort"), data.get("selectArrivePort"));

	}

}
