package com.qmetry.qaf.tests;

import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.RandomStringGenerator;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.pages.FlightConfirmationPage;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.FlightSummaryPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class FlightBookingPageTest extends WebDriverTestCase{
	

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifyFlightBookingPage")
	public void verifyFlightBookingPage(Map<String, String> data) {
		
		 HomePage homePage=new  HomePage();
			homePage.launchPage(null);
			
			homePage.SignIn(data.get("userName"), data.get("password"));
			FlightFinderPage finderPage=new FlightFinderPage();
			finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
			
			SelectFlightPage flightPage=new SelectFlightPage();
			flightPage.selectFlight("DEPART",data.get("departFlightName"));
			flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
			flightPage.btnReserveFlight();
			
			
			FlightSummaryPage summaryPage=new FlightSummaryPage();
			String departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
			Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
			String arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
			Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
			
			summaryPage.passengersDetails(" "," ", "Hindu");
			summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"), data.get("passengerCCExpMonth"),data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), data.get("passengerCCMidName"), data.get("passengerCCLastName"));
			summaryPage.billingAddressDetails(data.get("billingAddressOne"),data.get("billingAddressTwo"), data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"), data.get("billingAddressCity"),data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.alertHandler();
			FlightConfirmationPage confirmationPage=new FlightConfirmationPage();
			confirmationPage.jsClick(summaryPage.clkBuyFlight());
			confirmationPage.verifyDepartingDetails();
			
			homePage.getFlightsLink().click();
			finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
			
			flightPage.selectFlight("DEPART",data.get("departFlightName"));
			flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
			flightPage.btnReserveFlight();
			
			
			 departFlightName=summaryPage.getFlightDetails( data.get("selectDapartPort"), data.get("selectArrivePort"));
			 Validator.verifyThat("Vlaidatin Arrive flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
			 arriveFlightName=summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
			 Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
			
			String firstName=RandomStringGenerator.get(10, RandomizerTypes.LETTERS_ONLY);
			String lastName=RandomStringGenerator.get(10, RandomizerTypes.LETTERS_ONLY);
			summaryPage.passengersDetails(firstName,lastName, "Hindu");
			summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"), data.get("passengerCCExpMonth"),data.get("passengerCCExpYear"), firstName,"",lastName);
			summaryPage.billingAddressDetails(data.get("billingAddressOne"),data.get("billingAddressTwo"), data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"), data.get("billingAddressCity"),data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.alertHandler();
			summaryPage.clkBuyFlight();
			confirmationPage.verifyDepartingDetails();
	}
	
	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifyFlightBookingPageAfterEnteringRandomAlphabetName")
	public void verifyFlightBookingPageAfterEnteringRandomAlphabetName(Map<String, String> data) {

		HomePage homePage = new HomePage();
		homePage.launchPage(null);
		homePage.SignIn(data.get("userName"), data.get("password"));

		FlightFinderPage finderPage = new FlightFinderPage();
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
		
		SelectFlightPage flightPage = new SelectFlightPage();
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.btnReserveFlight();

		FlightSummaryPage summaryPage = new FlightSummaryPage();
		String departFlightName = summaryPage.getFlightDetails( data.get("selectDapartPort"),data.get("selectArrivePort"));
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		String arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));

		String firstName = RandomStringGenerator.get(10, RandomizerTypes.MIXED);
		String lastName = RandomStringGenerator.get(10, RandomizerTypes.MIXED);
		summaryPage.passengersDetails(firstName, lastName, "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"),firstName, "",
				lastName);
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.alertHandler();
		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		confirmationPage.jsClick(summaryPage.clkBuyFlight());
		confirmationPage.verifyDepartingDetails();
		
	}
	

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifyFlightBookingPageAfterEnteringRandomName")
	public void verifyFlightBookingPageAfterEnteringRandomName(Map<String, String> data) {

		HomePage homePage = new HomePage();
		homePage.launchPage(null);
		homePage.SignIn(data.get("userName"), data.get("password"));

		FlightFinderPage finderPage = new FlightFinderPage();
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));

		SelectFlightPage flightPage = new SelectFlightPage();
		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN",data.get("arriveFlightName"));
		flightPage.btnReserveFlight();

		FlightSummaryPage summaryPage = new FlightSummaryPage();
		String departFlightName = summaryPage.getFlightDetails(data.get("selectDapartPort"), data.get("selectArrivePort"));
		
		Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		String arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"), data.get("selectDapartPort"));
		Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));

		String firstName = RandomStringGenerator.get(10, RandomizerTypes.LETTERS_ONLY);
		String lastName = RandomStringGenerator.get(10, RandomizerTypes.LETTERS_ONLY);
		summaryPage.passengersDetails(firstName,lastName, "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), firstName, "",
				lastName);
		
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		
		summaryPage.alertHandler();
		FlightConfirmationPage confirmationPage = new FlightConfirmationPage();
		confirmationPage.jsClick(summaryPage.clkBuyFlight());
		confirmationPage.verifyDepartingDetails();
		
	}

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "BookFlightAndValidate")
	public void bookFlightAndValidate(Map<String, String> data) {
		
		 HomePage homePage=new  HomePage();
			Reporter.log("Opening the Given Applicaton");
			homePage.launchPage(null);
			
			homePage.SignIn(data.get("userName"), data.get("password"));
			
			FlightFinderPage finderPage=new FlightFinderPage();
			finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));
			
			SelectFlightPage flightPage = new SelectFlightPage();
			flightPage.selectFlight("DEPART",data.get("departFlightName"));
			flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
			flightPage.btnReserveFlight();

			FlightSummaryPage summaryPage = new FlightSummaryPage();
			String departFlightName = summaryPage.getFlightDetails( data.get("selectDapartPort"),data.get("selectArrivePort"));
			Validator.verifyThat("Vlaidatin depature flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
			String arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"),  data.get("selectDapartPort"));
			Validator.verifyThat("Vlaidatin Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
			
			summaryPage.passengersDetails(data.get("passengerCCFirstName"),data.get("passengerCCLastName"), "Hindu");
			summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"), data.get("passengerCCExpMonth"),data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), data.get("passengerCCMidName"), data.get("passengerCCLastName"));
			summaryPage.billingAddressDetails(data.get("billingAddressOne"),data.get("billingAddressTwo"), data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"), data.get("billingAddressCity"),data.get("billingAddressState"), data.get("billingAddressZipCode"), data.get("billingAddressCountry"));
			summaryPage.alertHandler();
			
			FlightConfirmationPage confirmationPage=new FlightConfirmationPage();
			confirmationPage.jsClick(summaryPage.clkBuyFlight());
			confirmationPage.verifyDepartingDetails();
	}
}
