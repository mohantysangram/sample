package com.qmetry.qaf.tests;

import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.FlightSummaryPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.SelectFlightPage;

public class PopUpMsgTest extends WebDriverTestCase {

	@QAFDataProvider(key = "verifyingHomePageDisplayedForLoggedUsers.data")
	@Test(description = "VerifyPopUpMessageUponChangingCountry")
	public void verifyPopUpMessageUponChangingCountry(Map<String, String> data) {

		Reporter.log("Validating the MOre Than 16 Digit for the CC Number");
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		homePage.SignIn(data.get("userName"), data.get("password"));
		FlightFinderPage finderPage = new FlightFinderPage();
		SelectFlightPage flightPage = new SelectFlightPage();
		FlightSummaryPage summaryPage = new FlightSummaryPage();

	
		homePage.getFlightsLink().click();
		finderPage.searchFlightForBooking(data.get("tripType"), data.get("selectPassangerCount"), data.get("selectDapartPort"),data.get("selectDapartMonth"), Integer.parseInt(data.get("selectDapartDay")), data.get("selectArrivePort"), data.get("selectarriveMonth"),Integer.parseInt(data.get("selectToDay")), data.get("serviceClass"), data.get("selectAirLinePreference"));

		flightPage.selectFlight("DEPART",data.get("departFlightName"));
		flightPage.selectFlight("RETURN", data.get("arriveFlightName"));
		flightPage.btnReserveFlight();

		String departFlightName = summaryPage.getFlightDetails( data.get("selectDapartPort"),data.get("selectArrivePort"));
		 Validator.verifyThat("validating depart flight name", departFlightName, Matchers.equalToIgnoringCase(data.get("departFlightName")));
		String arriveFlightName = summaryPage.getFlightDetails(data.get("selectArrivePort"), data.get("selectDapartPort"));
		 Validator.verifyThat("validating Arrive flight name", arriveFlightName, Matchers.equalToIgnoringCase(data.get("arriveFlightName")));
		

		summaryPage.passengersDetails(data.get("passengerCCFirstName"), data.get("passengerCCLastName"), "Hindu");
		summaryPage.credirCardDetails(data.get("CreditCardType"), data.get("CreditCardNumber"),
				data.get("passengerCCExpMonth"), data.get("passengerCCExpYear"), data.get("passengerCCFirstName"), "",
				data.get("passengerCCLastName"));
		summaryPage.billingAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		summaryPage.deliveryAddressDetails(data.get("billingAddressOne"), data.get("billingAddressTwo"),
				data.get("billingAddressCity"), data.get("billingAddressState"), data.get("billingAddressZipCode"),
				data.get("billingAddressCountry"));
		String popUpMsg=summaryPage.alertHandler();
		 Validator.verifyThat("validating the Pop Up Message", popUpMsg, Matchers.containsString("You have chosen a mailing location outside of the United States and its territories. An additional charge of $6.5 will be added as mailing charge."));
		String countryName=summaryPage.selectPassengerDeliveryAddressCountry().getText();
		Reporter.log("Country Name is :"+countryName);
		 Validator.verifyThat("both country name should matched",countryName, Matchers.containsString(data.get("billingAddressCountry")));
	}
}
