package com.qmetry.qaf.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.pages.FlightFinderPage;
import com.qmetry.qaf.pages.HomePage;
import com.qmetry.qaf.pages.UnderConstructionPage;

public class HomePageTest extends WebDriverTestCase {

	@QAFDataProvider(dataFile = "resources/TestData/verifyingHomePageDisplayedAndComponents.csv")
	@Test(description = "verifyingHomePageDisplayedAndComponents",priority=1)
	public void verifyingHomePageDisplayedAndComponents(Map<String,String>data) {

		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);

		homePage.getHomePageLogo().verifyVisible("Validating the App Logo");

		homePage.getSignInLink().waitForPresent();
		homePage.getSignInLink().verifyVisible("Validating the SignIn Link");

		homePage.getRegisterLink().verifyVisible("Validating the Register Link Present");

		homePage.getSupportLink().verifyVisible("Validating the Support Link Present");

		homePage.getContactLink().verifyVisible("Validating the Contact Link Present");

		homePage.getHomeLink().verifyVisible("Validating the Home Link Present");

		homePage.getFlightsLink().verifyVisible("Validating the Flights Link Present");

		homePage.getHotelLink().verifyVisible("Validating the Hotels Link Present");

		homePage.getCarRentalsLink().verifyVisible("Validating the Car Rentals Link Present");

		homePage.getCruisesLink().verifyVisible("Validating the Cruises Link Present");

		homePage.getDestinationLink().verifyVisible("Validating the Destinations Link Present");

		homePage.getVacationLink().verifyVisible("Validating the Vacations Link Present");

		homePage.SignIn(data.get("UserName"),data.get("Password"));
		homePage.waitForPageToLoad();
		

	}
	
	@QAFDataProvider(dataFile = "resources/TestData/verifyingHomePageDisplayedAndComponents.csv")
	@Test(description = "verifyingHomePageDisplayedForLoggedUsers",priority=2)
	public void verifyingHomePageDisplayedForLoggedUsers(Map<String,String>data) {

		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		
		homePage.SignIn(data.get("UserName"),data.get("Password"));
		homePage.waitForPageToLoad();
		Reporter.log("Page is loaded");
		
		FlightFinderPage finderPage=new FlightFinderPage();
		finderPage.getFindFlightLogo().verifyVisible("Flight Finder Logo Is visible After Logged In");
	}
	
	@Test(description = "VerifyContactLink")
	public void verifyContactLink() {
		
		HomePage homePage = new HomePage();
		Reporter.log("Opening the Given Applicaton");
		homePage.launchPage(null);
		homePage.btnContact();
		
		UnderConstructionPage constructionPage=new UnderConstructionPage();
		constructionPage.getTxtInconvieneceMsg().verifyVisible("Sorry for any inconvienece.");
		constructionPage.getbtnBackToHome().verifyVisible("Back To home Button Is Displayed");
		
	}

}
